import React from 'react';

export default function Table(props) {

  // If the user has entered some employee names, display them in a table. 
  // Else prompt the user to enter some data. 

    if (props.names.length > 0) {
      return (
      <div className="table">
      <h2>Employees</h2>
      <table>
        <thead>
          <tr>
            <th>Name</th>
            <th>Email address</th>
          </tr>
        </thead>
        <tbody>
          {props.names.map((nameRef) => 
          <tr key={nameRef.id}> {/* Row holding employee record */}

              {/* Ternary operator: if name is being edited, show an input. Else display the name as entered. */}
              {props.nameRefEditing === nameRef.id ? (
                <td><input 
                type="text" 
                onChange={(event) => props.setEditingText(event.target.value)} 
                value={props.editingText} required/></td>) 
                : (<td>{nameRef.text}</td>) 
                }

              {/* Ternary operator: if email address is being edited, show an input. Else display the email address as entered. */}
                {props.emailRefEditing === nameRef.id ? (
                  <td><input
                  type="email"
                  onChange={(event) => props.setEditingEmail(event.target.value)}
                  value={props.editingEmail} required/></td>)
                : (<td>{nameRef.email}</td>)
                }   

            {/* Button: Edit employee name */}
            {props.nameRefEditing === nameRef.id ? (
            <td><button onClick={() => props.editNameRef(nameRef.id)}>Submit edit</button></td>) 
            : (<td><button onClick={() => props.setNameRefEditing(nameRef.id)}>Edit name</button></td>)
            }

            {/* Button: Edit employee email address */}
            {props.emailRefEditing === nameRef.id ? (
            <td><button onClick={() => props.editEmailRef(nameRef.id)}>Submit edit</button></td>) 
            : (<td><button onClick={() => props.setEmailRefEditing(nameRef.id)}>Edit email</button></td>)
            }

            {/* Button: Delete employee record */}
            <td><button className="delete-button" onClick={() => props.deleteNameRef(nameRef.id)}>Delete employee record</button></td>
            
          </tr>)}
          {/* Button: Alphabetise nameList */}
          <tr>
            <td><button type="submit" className="alph-button" onClick={() => props.alphabetize()}>Alphabetize employees</button></td>
          </tr>

        </tbody>
      </table>
    </div> )
    } else { 
      return (
        <div class="table-empty"><h2>No data yet. Please enter employee details.</h2></div>
      )
    }

}
