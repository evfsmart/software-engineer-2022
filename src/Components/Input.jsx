import React from 'react';

const Input = ({ parentRef }) => {
  return (
    <>
      <input ref={parentRef} />
    </>
  )
};

export default Input;
