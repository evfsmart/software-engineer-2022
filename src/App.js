import React, { useState} from 'react';
import "./styles.css"
import Table from './Components/Table';

function App() {

  const [nameList, setNameList] = useState([])
  const [nameRef, setNameRef] = useState("")
  const [emailRef, setEmailRef] = useState("")
  const [nameRefEditing, setNameRefEditing] = useState(null) //id of nameRef being edited
  const [editingText, setEditingText] = useState("") // hold nameRef editing value
  const [emailRefEditing, setEmailRefEditing] = useState(null)
  const [editingEmail, setEditingEmail] = useState("") // hold emailRef editing value

  // Retrieve state// 
  React.useEffect(() => {
    const temp = localStorage.getItem("employeeList")
    const loadedEmployeeList = JSON.parse(temp)

    if(loadedEmployeeList) {
      setNameList(loadedEmployeeList)
    }
  },[])

    // Save state // Function will run every time nameList is updated
    React.useEffect(() => {
      const temp = JSON.stringify(nameList)
      localStorage.setItem("employeeList", temp)
    }, [nameList])


  // Edit employee name //
  function editNameRef(id) {
    const updatedNameList = [ ...nameList].map((nameRef) => {
      if (nameRef.id === id) {
        nameRef.text = editingText
      }
      return nameRef
    })
    setNameList(updatedNameList)
    setNameRefEditing(null)
    setEditingText("")
  }

  // Edit employee email address //
  function editEmailRef(id) {
    const updatedNameList = [ ...nameList].map((nameRef) => {
      if (nameRef.id === id) {
        nameRef.email = editingEmail
      }
      return nameRef
    })
    setNameList(updatedNameList)
    setEmailRefEditing(null)
    setEditingEmail("")
  }

  // Delete all details for employee instance // 
  function deleteNameRef(id) {
    const updatedNameList = [ ...nameList].filter((nameRef) => nameRef.id !== id)
    setNameList(updatedNameList)
  }

  // Alphabetize // 
  function alphabetize() {
    let nameListClone = [...nameList]
    nameListClone.sort((a,b) => {
      if (a.text < b.text) {
        return -1;
      }
      if (a.text > b.text) {
        return 1
      }
      return 0;
    });
    setNameList(nameListClone)
} 

  // Initial function on load of form: //
  function handleSubmit(event) {
    event.preventDefault();

    // Stop "New employee name" field from accepting numbers or punctuation //
    var letters = /^[A-Za-z\s]+$/;
    if(nameRef.match(letters)) {
      const newNameRef = {
        id: new Date().getTime(), //to create unique id 
        text: nameRef,
        email: emailRef,
      }

      setNameList([ ...nameList].concat(newNameRef))
      setNameRef("")
      setEmailRef("")
    } else {
      alert("Employee name must be letters only.")
    }
  }

  // Page structure // 
  return (
    <div className="App">
      
      <div className="heading">
        <h1>Human Resources</h1>
      </div>

      <div className="container">

      <form onSubmit={handleSubmit}>
        <div className="form-field">
          <label for="nameRef">New employee name:</label><br />
          <input className="input" type="text" onChange={(event) => setNameRef(event.target.value)} value={nameRef} required/>
        </div>
        <div className="form-field">
          <label for="emailRef">New employee email:</label><br />
          <input className="input" type="email" onChange={(event) => setEmailRef(event.target.value)} value={emailRef} required/>
        </div>
        <button type="submit">Add new record</button>
      </form>

      <Table 
      names={nameList} 
      deleteNameRef={deleteNameRef}
      alphabetize={alphabetize}

      nameRefEditing={nameRefEditing}
      setNameRefEditing={setNameRefEditing}
      editingText={editingText}
      setEditingText={setEditingText}
      editNameRef={editNameRef}

      emailRefEditing={emailRefEditing}
      setEmailRefEditing={setEmailRefEditing}
      editingEmail={editingEmail}
      setEditingEmail={setEditingEmail}
      editEmailRef={editEmailRef}
      />
    </div>
    <div className="footer">Emma Smart | emmadevelops@outlook.com</div>
    </div>
  );
}

export default App;
